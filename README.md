
Shadow Light - Théo Vidal - Tout droit reservé, interdit à la redistribution ou recréation

Shadow Light est un Afficheur spectrographe multi-fonctions pour clavier Razer Chroma, Corsair RGB, et bandes LED RVB pour Windows et Linux

ATTENTION
Vous devez posseder Microsoft Visual C++ 2015 32-bit redistributable package installé pour executer le programme (Windows).

Pour Construire le programme sur Linux, installez QT Creator et libopenal-dev, libhidapi-dev et ouvrez/compilez ficher .pro .

# Paramètres

Il y a de nombreux paramètres pour personnaliser au mieux le programme

    * Puissance - Combien le programme réagit au son.  Si il ne répond pas ou peu, augmentez cette valeur.
    * Puissance de fond - A quelle luminosité le fond est. (0-100%)
    * Taille de l'onde - Epaisseur des barres, Plus il y en a, plus les detailles du son seront précis selon
                        le nombre de LED sur votre/vos appareil/s.
    * Decay - Combien la valeur du son se retracte. (0-100%)
    * Delai - Combien de millisecondes entre chaque actualisation. Moins la valeur est haute plus les "FPS" seront meilleurs
                        mais cela peut causer des ralentissments sur certains appareils.

    * Normalization Offset - Ajuster la valeur des basses.
    * Normalization Scale - Epaisseur des basses.
    * Filter Constant - Fluidité des barres.
    * FFT mode fenetre - Selectionnez entre None, Hamming, Hanning, and Blackman pour les fonctions de fenetres.
                        (voir https://en.wikipedia.org/wiki/Window_function)
    * Mode de fond - Selectionnez entre des couleurs ou des patternes statiques ou animé variés.
    * Mode actif - Selectionnez entre des couleurs ou patternes variés au premier plan.
    * Mode couleur unique - Selectionnez entre des options variées pour les appareils utilisants un effet à seul couleur. (ex: Casques)
    * Lissage - Selectionnez entre des barres fidèles (binning) ou lisses (low pass).
    * Vitesse de l'Animation - Vitesse des animations ou des patternes pour le fond ou le premier plan, 100 par defaut,
                        peut être négatif pour inverser le sens du patternes, peut être augmenté pour être plus rapide ou diminué pour le ralentir.
    * Retour de fond - Combien faut-il attendre (en multiples de 10 millisecondes) avant que le fond se nuance au premier plan.

# Fichier "Settings"

Shadow Light vous permet d'enregistrer votre paramètres. Les commandes de fichiers sont les suivants:


        amplitude         - Puissance du son reçu
        bkgd_bright       - Ajuster la luminosité du fond
        avg_size          - Nombre de la valeur des barres
        delay             - Millisecondes entre les actualisations des appareils
        bkgd_mode         - Mode (animations ou patternes) du fond:
                          - 0  Noir
                          - 1  Blanc
                          - 2  Rouge
                          - 3  Orange
                          - 4  Jaune
                          - 5  Vert
                          - 6  Cyan
                          - 7  Bleu
                          - 8  Violet
                          - 9  Vert/Jaune/Rouge
                          - 10 Vert/Blanc/Rouge
                          - 11 Bleu/Cyan/Blanc
                          - 12 Rouge/Blanc/Bleu
                          - 13 Barres arc-en-ciel
                          - 14 Barres arc-en-ciel Inversé
                          - 15 Original
                          - 16 Arc-en-ciel
                          - 17 Roue de couleur
                          - 18 Couleur spectrum
        frgd_mode         - Couleur du premier plan:
                          - 0  Noir
                          - 1  Blanc
                          - 2  Rouge
                          - 3  Orange
                          - 4  Jaune
                          - 5  Vert
                          - 6  Cyan
                          - 7  Bleu
                          - 8  Violet
                          - 9  Vert/Jaune/Rouge
                          - 10 Vert/Blanc/Rouge
                          - 11 Bleu/Cyan/Blanc
                          - 12 Rouge/Blanc/Bleu
                          - 13 Barres arc-en-ciel
                          - 14 Barres arc-en-ciel Inversé
                          - 15 Original
                          - 16 Arc-en-ciel
                          - 17 Roue de couleur
                          - 18 Couleur spectrum
        single_color_mode - Single color mode, values are:
                          - 0  Black
                          - 1  White
                          - 2  Red
                          - 3  Orange
                          - 4  Yellow
                          - 5  Green
                          - 6  Cyan
                          - 7  Blue
                          - 8  Purple
                          - 9  Background
                          - 10 Follow Background
                          - 11 Follow Foreground
        anim_speed        - Vitesse de l'Animation (pourcent)
        server            - Configurez ce programme comme un serveur pour une synchronisation
                            - Port pour établir la connection (1234 par defaut), i.e. server=1234
        client            - Configurez ce programme comme un esclave pour une synchronisation
                             - Ecrivez l'IP et le port du serveur pour établir la connection,
                             - i.e. client=192.168.1.X,1234
        ledstrip          - Commandes pour les LEDs :
                          - Devise : ledstrip=port,actualisation,nombre_leds
                             - (ex.ledstrip=COM1,115200,30)
                             - UDP (en local par internet) : ledstrip=udp:client,port,num_leds
                             - (ex.ledstrip=udp:192.168.1.5,1234,30)
        xmas              - COM port, ex. xmas=COM2

# Effects Visuels Disponibles

    * Spectrographe - Affiche des barres verticales pour chaque frequences du son.
    * Barre - Affiche une seul barre represantent les frequences des basses.
    * Couleur seul - Luminoisté et couleur represantent la force et les frequences des basses.

# Appareils supporté (Windows)

    * Razer Chroma SDK

        Claviers
        - BlackWidow Chroma (spectrographe)
        - BlackWidow Chroma Tournament Edition (spectrographe)
        - DeathStalker Chroma (barre horisontal)
        - BlackWidow X Chroma (spectrographe)
        - BlackWidow X Chroma Tournament Edition (spectrographe)
        - Razer Blade Stealth (spectrographe)
        - Razer Blade (spectrographe)
        - Razer Ornata Chroma (spectrographe)
        - Razer Lancehead Tournament Edition (spectrographe)
        - Razer Lancehead (spectrographe, peut être plus lent/saccadé)

        Keypads
        - Razer Orbweaver Chroma (spectrographe)
        - Razer Tartarus Chroma (Une seul couleur)

        Souris
        - Diamondback Chroma (barres & couleur seul)
        - Mamba Tournament Edition (barres & couleur seul)
        - Mamba Chroma Wireless (barres & couleur seul, avec ou sans fil)
        - Razer Lancehead Tournament Edition (spectrographe)
        - Razer Lancehead (spectrographe, avec ou sans fil, peut être plus lent/saccadé)
        - DeathAdder Chroma (couleur seul)
        - Naga Epic Chroma (couleur seul, mode avec fil seulement)
        - Naga Chroma (couleur seul)
        - Orochi Chroma (couleur seul, mode avec fil seulement)

        Casques
        - Kraken 7.1 Chroma (couleur seul)
        - Kraken 7.1 Chroma V2 (couleur seul)
        - ManO'War (couleur seul)

        - The Kraken 7.1 Chroma V1 peut être saccadé voir incompatible

        Tapis de souris
        - Firefly (barres)
        - Goliathus (Une seul couleur)

        Carte graphique externe
        - Razer Core (barres et couleur seul)

        Autres
        - Razer Chroma Mug (barres)
        - Philips Hue integration complete (couleur seul)

    * Corsair CUE SDK

        Claviers
        - K70 RGB (spectrographe)
        - Tout les autre claviers compatible CUE (spectrographe), non testé

    * Cooler Master RGB

    	Claviers
    	- Masterkeys Pro L (spectrographe)
    	- Masterkeys Pro S (spectrographe)

    * Logitech G Gaming SDK

        Claviers
        - G910 Orion Spark (spectrographe)
        - G810 Orion Spectrum (spectrographe)
        - Autres appareils avec des couleurs RVB (couleur seul)

        Casques
        - G633 (couleur seul)
        - G933 (couleur seul)

        Souris
        - G600 (couleur seul)
        - G900 Chaos Spectrum (couleur seul)
        - G303 Daedalus Apex (couleur seul)

    * SteelSeries

        Claviers
        - Apex M800 (spectrographe)

    * MSI SteelSeries (ordinateur portable)

        Claviers
        - MSI 3-zones RGB (MSI GS63VR, etc) (barres, couleur seul)

    * NZXT Hue+

        - Configurez avec la commande hueplus=port,num_leds dans le fichier "Settings" (barres)

    * WS28XX Pixel LED Strips

        - Arduino (Serial/USB-Serial) ou ESP8266 (UDP en WiFi) utilisé en controlleur
        - WS2812B, WS2811, et les autres LED strips/strings supporté
        - 90 LEDs maximum recommandé, utilisez un sketch arduino avec 115200 baud rate.
        - Pour les LEDs par USB, -ledstrip=port,baud,nombres_leds (ex. -ledstrip=COM1,115200,30)
        - Pour les LEDs par WIFI (internet en local), -ledstrip=client,port,nombres_leds (ex. -ledstrip=192.168.1.5,1234,30)
        - Pour plus de 200 LEDs sur Arduino, uilisez 1000000 de baud rate

# Appareils supportés (Linux)

    * Razer Linux Drivers (https://github.com/terrycain/razer-drivers)

        Claviers
        - BlackWidow Chroma (spectrographe)
        - BlackWidow Chroma Tournament Edition (spectrographe)
        - DeathStalker Chroma (horisontale barres)
        - Razer Ornata Chroma (spectrographe)
        - Razer Blade Stealth (spectrographe)
        - Razer Blade Pro (spectrographe)

        Keypads
        - Razer Tartarus Chroma (single color)

        Souris
        - Diamondback Chroma (bar and single color)
        - Mamba Tournament Edition (bar and single color)
        - DeathAdder Chroma (single color)

        Casques
        - Kraken 7.1 Chroma (single color)
        - Kraken 7.1 Chroma V2 (single color)

        Tapis de soursi
        - Firefly (bar)

        Carte graphique externe
        - Razer Core (bar and single color)

        Autres
        - Razer Chroma Mug (bar)

    * SteelSeries

        Claviers
        - Apex M800 (spectrograph)

    * MSI SteelSeries

        Claviers
        - MSI 3-zone (MSI GS63VR, etc) (bar, single color)

    * WS28XX Pixel LED Strips

        - Voir les infos sur les appareils compatibles Windows
